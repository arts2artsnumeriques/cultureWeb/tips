## Php  
- ### Run php without apache
	In your shell: 
	``` bash
	php -S localhost:8000
	```
	if you want start at a different root:
	``` bash
	php -S localhost:8000 -t </path/to/root>
	```
- ### Regex for type (french)
  ``` php
  function rft($content) {
  	$thinsp = '<span class="thinsp">&nbsp;</span>';
  	$regex = array(
    	"/ ;/"	  => ';',
      "/« /"	  => '«',
    	"/“ /"	  => '«',
    	"/ »/"	  => '»',
    	"/ ”/"	  => '»',
    	"/ :/"	  => ':',
    	"/ !/"	  => '!',
      "/ €/"    => '€',
      "/ $/"    => '$',
      "/ £/"    => '£',
    	"/ \?/"	  => '?',
    	"/:/" 	  => $thinsp . ':',
      "/«/"     => '«' . $thinsp,
    	"/“/" 	  => '«' . $thinsp,
      "/»/" 	  => $thinsp . '»',
      "/€/"     => $thinsp . '€',
      "/$/"     => $thinsp . '$',
      "/£/"     => $thinsp . '£',
    	"/”/" 	  => $thinsp . '»',
    	"/!/" 	  => $thinsp . '!',
    	"/\?/"	  => $thinsp . '?',
    	"/& /"	  => '&amp; ',
    	"/oe/"	  => '&#339;',
    	"/ae/"	  => '&#230;',
    	"/OE/"	  => '&#338;',
    	"/AE/"	  => '&#198;',
    	"/\.\.\./"=> '&#8230;',
        # url bug correction
    	'/http<span class="thinsp">&nbsp;<\/span>:/' => 'http:',
    	'/https<span class="thinsp">&nbsp;<\/span>:/' => 'https:',
    	'/href="mailto<span class="thinsp">&nbsp;<\/span>:/' => 'href="mailto:',
    	'/watch<span class="thinsp">&nbsp;<\/span>?/' => 'watch',
   });
      
    foreach ($regex as $input => $output) {
  		$content = preg_replace($input, $output, $content);
      }
      return $content;
    }
  ```

  Then use the css property `letter-spacing` on `.thinsp` to set the width value of your thinspace. 

- ### Regex for type (english)
  ``` php
  function rft($content) {
    $thinsp = '<span class="thinsp">&nbsp;</span>';
    $regex = array(
        "/ ;/"    => ';',
        "/« /"    => '«',
        "/“ /"    => '“',
        "/ »/"    => '»',
        "/ ”/"    => '”',
        "/ :/"    => ':',
        "/ !/"    => '!',
        "/ €/"    => '€',
        "/ $/"    => '$',
        "/ £/"    => '£',
        "/ \?/"   => '?',
        "/«/"     => '«' . $thinsp,
        "/»/"     => $thinsp . '»',
        "/€/"     => $thinsp . '€',
        "/$/"     => $thinsp . '$',
        "/£/"     => $thinsp . '£',
        "/& /"    => '&amp; ',
        "/oe/"    => '&#339;',
        "/ae/"    => '&#230;',
        "/OE/"    => '&#338;',
        "/AE/"    => '&#198;',
        "/\.\.\./"=> '&#8230;',
          # url bug correction
        '/http<span class="thinsp">&nbsp;<\/span>:/' => 'http:',
        '/https<span class="thinsp">&nbsp;<\/span>:/' => 'https:',
        '/href="<main></main>ilto<span class="thinsp">&nbsp;<\/span>:/' => 'href="mailto:',
        '/watch<span class="thinsp">&nbsp;<\/span>?/' => 'watch',
       });

      foreach ($regex as $input => $output) {
            $content = preg_replace($input, $output, $content);
      }
      return $content;
  }
  ```
Then use the css property `letter-spacing` on `.thinsp` to set the width value of your thinspace. 

