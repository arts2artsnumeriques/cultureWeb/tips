
## Git

### Les bases de git

#### Ajouter toutes les modifications
``` bash
git add .
 ```
#### pour cibler le dossier que je veux modifier
``` bash
git add  ‘chemin/vers/le/fichier’ 
```

#### git status (vérification)
``` bash
git status
``` 

#### git pull (récupérer les dernières modifications)
``` bash
git pull
``` 
#### git commit -m ‘amélioration du formulaire’
``` bash
git commit -m
``` 
#### git push
``` bash
git commit -m
``` 
