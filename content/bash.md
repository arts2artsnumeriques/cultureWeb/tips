# Bash 
**ou comment écrire un script qui update automatiquement en ligne de commande ton ordinateur sous Ubuntu**

### Bash

Tout d'abord, il faut savoir que la distribution Ubuntu utilise le bash dans le terminal.

Voici deux liens qui te permttrons d'appronfondir le sujet:
  >[https://fr.wikipedia.org/wiki/Bourne-Again_shell](https://fr.wikipedia.org/wiki/Bourne-Again_shell)

  >[https://help.ubuntu.com/community/Beginners/BashScripting](https://help.ubuntu.com/community/Beginners/BashScripting)

Mais sache donc que l'on peut faire la même chose dans le terminale et l'interface graphique de ton OS (Gnome pour Ubuntu).

### les commandes pour mettre à jour sous Ubuntu

Souvent les application qui sont dans l'Ubuntu Store sont pas à la dernière version du logiciel.
Depuis peu, il y a d'autre App store compatible, telle que [Snap](https://snapcraft.io/) ou [Flatpack](https://flatpak.org/), mais c'est pas forcement maintenue et mis à jours par les devellopeurs du logiciel.

Or, il existe encore une autre façon d'avoir accès à la toute dernière version du logiciel (pour ubuntu): le ppa. Je ne vais pas m'étendre sur le sujet, mais on peut directement ajouter un ppa, un liens qui permet de garder à jours les applications que l'on installe (souvent manuellement).

ça ressemble souvent à quelque chose comme ceci:

```bash sudo add-apt-repository ppa: ...```

Lorsqu'on ajoute un ppa, il faut mettre à jours la list de source de mise à jours.
Pour cela, on utilise la commande:

```bash sudo apt update```

puis on installe les mises à jours en utilisant la commande :

```bash sudo apt upgrade```

Et au finale, pour nettoyer le système , on peut lancer la commande:

```bash sudo apt autoremove```

Cela enlève les paquets dont le système n'a plus besoin.

#### Note sur la syntax ```sudo```
Le sudo permet d'avoir un accès root pour la commande. L'accès root permet d'avoir un accès Superuserà tout le système. Si la commande est exécuté sans vérifier ce qu'elle va faire, il peut avoir des effets indésirables.

### Un script pour tout automatiser

Voilà la chose super sympa, on peut faire des scripts en bash. Cela vas nous permettre dans ce cas là de lancer toute les commandes si dessus en une ligne de commande, qui lanceras le script.

Tout d'abord, il faut dire au système que le script est en bash. Pour celà, on vas plaer au tout début du script la ligne suivante:

```#!/bin/bash```

Et voilà. On peut maintenant mettre à la suite tout les commandes pour updater son ordi en un rien de temps (je devrais faire de pub pour Castorama à ce rythme). Il faut juste pas oublier d'enregistrer le script avec comme extension de fichier le ```.sh```.

Voici à quoi donc le script en entier:

```bash
#!/bin/bash

sudo apt update
sudo apt upgrade
sudo apt autoremove
```


Cependant, je conseil de le faire de temps en temps à la mains pour ne pas oublier comment le faire si besoin est. Et surtout, avec le bash( que l'on retrouve aussi sur IOS), on peut faire beaucoup de chose.

