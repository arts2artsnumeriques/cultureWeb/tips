
* __Terminal emulator__
	* [terminator](https://gnometerminator.blogspot.com/p/introduction.html)
* __Shell__
	* [fish](https://fishshell.com/)
* __Editor__
	* [vim](https://www.vim.org/) (for code)
	* [ghostwriter](https://github.com/wereturtle/ghostwriter) (for markdown -> memo and doc)
* __Communication__
	* [telegram-desktop](https://telegram.org/)
	* [telegram-cli](https://github.com/vysheng/tg)
	* [SkypeForLinux](https://www.ubuntuupdates.org/ppa/skypeforlinux) (non free)
	* [irssi](https://irssi.org/) (irs cli client)
* __Mail__
	* [thunderbird](https://www.thunderbird.net/fr/)
* __Calendar__
	* [korganizer](https://userbase.kde.org/KOrganizer)
* __Image Viewer__
	* [feh](https://feh.finalrewind.org/) (very ligthweigh !)
	* [okular](https://okular.kde.org/)
* __Video Player__
	* [parole](https://docs.xfce.org/apps/parole/start)
	* [mpv](https://mpv.io/installation/)
* __Music Player__
	* [cmus](https://cmus.github.io/) (cli)
* __Display Manager__
	* [lightdm](https://doc.ubuntu-fr.org/lightdm)
* __Window manager__
	* [i3](https://i3wm.org/)
	* [xfce](https://xfce.org/)
* __File Manager__
	* [nnn](https://github.com/jarun/nnn) (cli)
	* [pcmanfm](https://doc.ubuntu-fr.org/pcmanfm)
* __FTP client__
	* [filezilla](https://filezilla-project.org/)
* __Web Browser__
	* firefox
	* chromium
* __Laptop Power Saving__
	* [tpl](https://linrunner.de/en/tlp/tlp.html)
* __Offline Documentation Browser__
	* [zeal](https://zealdocs.org/)
* __Partition Editor__
	* [gparted](https://gparted.org/)
	* [cfdisk](https://www.computerhope.com/unix/cfdisk.htm)
* __Screenshot__
	* [scrot](https://opensource.com/article/17/11/taking-screen-captures-linux-command-line-scrot)
* __Podcast player__
	* [Clementine](https://www.clementine-player.org/)
* __BitTorrent client__
	* [rtorrent](https://github.com/rakshasa/rtorrent/wiki) (cli)
* __Output Screen__
	* [xrandr](https://wiki.archlinux.org/index.php/Xrandr)
	* [arandr](http://debian-facile.org/doc:environnements:x11:arandr)
* __Conversion Formats__
	* [pandoc](https://pandoc.org/) 
* __Grammar and typographic checker__
	* [Grammalecte](https://grammalecte.net/)
